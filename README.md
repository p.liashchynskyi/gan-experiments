# Cyt

## DCGAN

`CUDA_VISIBLE_DEVICES=0 nohup python src/main.py -t -hdf5 -l -batch_stat -metrics is fid prdc -cfg ./src/configs/Custom/DCGAN_CYT.yaml -data ./data/cyt_64 -save ./save/dc_cyt -v -knn -fa -tsne -sf -sf_num 300 > dcgan_cyt.log &`

## WGAN-WC

`CUDA_VISIBLE_DEVICES=0 nohup python src/main.py -t -hdf5 -l -batch_stat -metrics is fid prdc -cfg ./src/configs/Custom/WGAN-WC_CYT.yaml -data ./data/cyt_64 -save ./save/wgan_wc_cyt -v -knn -fa -tsne -sf -sf_num 300 > wgan_wc_cyt.log &`

## BIGGAN

`CUDA_VISIBLE_DEVICES=0 nohup python src/main.py -t -hdf5 -l -batch_stat -metrics is fid prdc -cfg ./src/configs/Custom/BIGGAN_CYT.yaml -data ./data/cyt_64 -save ./save/biggan_cyt -v -knn -fa -tsne -sf -sf_num 300 > biggan_cyt.log &`

# Hist

## DCGAN

`CUDA_VISIBLE_DEVICES=0 nogup python src/main.py -t -hdf5 -l -batch_stat -metrics is fid prdc -cfg ./src/configs/Custom/DCGAN_HIST.yaml -data ./data/hist_64 -save ./save/dc_hist -v -knn -fa -tsne -sf -sf_num 300 > dcgan_hist.log &`

# IGH

## DCGAN

`CUDA_VISIBLE_DEVICES=0 nohup python src/main.py -t -hdf5 -l -batch_stat -metrics is fid prdc -cfg ./src/configs/Custom/DCGAN_IGH.yaml -data ./data/igh_64 -save ./save/dc_igh -v -knn -fa -tsne -sf -sf_num 300 > dcgan_igh.log &`


# GENERATE

`python src/main.py -cfg ./src/configs/Custom/DCGAN_CYT.yaml -hdf5 -l -save ./save/dc_cyt -v -knn -fa -tsne -sf -sf_num 300 -ckpt ./save/dc_cyt/checkpoints/cyt_64-DCGAN_CYT-train-2022_04_02_15_47_48 -data ./data/cyt_64 -ref="valid"`
